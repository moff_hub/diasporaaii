from flask import Flask, render_template, url_for, request

app = Flask(__name__)
#landing route
@app.route('/')
def landing ():
    return render_template('landing.html')
# login route definition and allowed methods
@app.route('/login', methods=['POST','GET'])
def login():
    if request.method == 'POST':
        pass
    return render_template('login.html')
# register route
@app.route('/register', methods=['POST','GET'])
def register():
    if request.method == 'POST':
        pass
    return render_template('register.html')
#dashboard
@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')
#start the app at port 7000
# run in debug mode
if __name__ == '__main__':
   app.run(debug=True,port=7000)
